# Python Space Packet

The CCSDS Space Packet Protocol (SPP) was developed by the
[Consultative Committee for Space Data Systems (CCSDS)](https://public.ccsds.org):

> The Space Packet Protocol is designed to meet the requirements of space missions to
efficiently transfer space application data of various types and characteristics over a network
that involves a ground-to-space or space-to-space communications link (also called
space link).

>The figure below illustrates where the Space Packet Protocol is located in the protocol stack. The
Space Packet Protocol provides a unidirectional data transfer service from a single source
user application to one or more destination user applications through one or more
subnetworks. The path from the source user application to the destination user application(s)
through the subnetwork(s) is called a Logical Data Path (LDP).

![](docs/assets/protocol_configuration.png)

This Python module is an implementation of the CCSDS Space Packet Protocol.
It follows the recommendations given by the latest version of the [CCSDS Blue Book](docs/133x0b1c2.pdf).

## Installation

Clone the repository and then install via pip:

```
$ git clone https://gitlab.com/librecube/lib/python-spp
$ cd python-spp
$ virtualenv venv
$ . venv/bin/activate
$ pip install -e .
```

## Example

See the [examples](./examples/README.md) for how to make use of this module.

## Contribute

- Issue Tracker: https://gitlab.com/librecube/lib/python-spp/-/issues
- Source Code: https://gitlab.com/librecube/lib/python-spp

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
