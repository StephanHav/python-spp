APID_IDLE_PACKET = 0b11111111111


class PacketType:
    IDLE = 0
    TELEMETRY = 0
    TELECOMMAND = 1


class SequenceFlags:
    CONTINUATION_SEGMENT = 0b00
    FIRST_SEGMENT = 0b01
    LAST_SEGMENT = 0b10
    UNSEGMENTED = 0b11


class ServiceType:
    PACKET_SERVICE = "PACKET SERVICE"
    OCTET_STRING_SERVICE = "OCTET STRING SERVICE"
