# Examples

Find here examples on how to use the SPP package.

## UDP Transport

The UDP transport example sends space packets from entity A (local) to B (remote) and vice versa
using UDP frames. For this the routing is set up to go over a waypoint.
In addition, there is a sink that simply reports the received packets.

So the routing is (APID is given in brackets):

A (111) <---> Waypoint (222) <---> B (333)
         ┖--> Sink (444)     <--┘

To run the example, in seperate terminals do:

```
$ python local.py
$ python waypoint.py
$ python sink.py
$ python remote.py
```
