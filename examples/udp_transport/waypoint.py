import spp
from spp.transport import UdpTransport


udp_transport = UdpTransport(
    routing={
        "*": [("127.0.0.1", 5111), ("127.0.0.1", 5333)],
        ("127.0.0.1", 5111): [("127.0.0.1", 5333)],
        ("127.0.0.1", 5333): [("127.0.0.1", 5111)],
    }
)

udp_transport.bind("127.0.0.1", 5222)

spp_entity = spp.SpacePacketProtocolEntity(apid=222, transport=udp_transport)


def display_received_packet(space_packet):
    print("Received Space Packet:", space_packet)
    attrs = vars(space_packet)
    print("   " + ", ".join("%s: %s" % item for item in attrs.items()))


spp_entity.indication = display_received_packet

input("Running. Press <Enter> to stop...\n")

udp_transport.unbind()
