import time

import spp
from spp.transport import UdpTransport


udp_transport = UdpTransport(
    routing={
        "*": [("127.0.0.1", 5222), ("127.0.0.1", 5444)],
    }
)

udp_transport.bind("127.0.0.1", 5111)

spp_entity = spp.SpacePacketProtocolEntity(apid=111, transport=udp_transport)


def display_received_packet(space_packet):
    print("Received Space Packet:", space_packet)
    attrs = vars(space_packet)
    print("   " + ", ".join("%s: %s" % item for item in attrs.items()))


spp_entity.indication = display_received_packet

input("Press <Enter> to start sending packets. Then press <Ctr-C> to stop.\n")

space_packet = spp.SpacePacket(
    packet_type=spp.PacketType.TELECOMMAND,
    packet_sec_hdr_flag=False,
    apid=333,
    sequence_flags=spp.SequenceFlags.UNSEGMENTED,
    packet_sequence_count=1,
    packet_data_field=b"This is a test packet!",
)

while True:
    try:
        spp_entity.request(space_packet)
        time.sleep(1)
    except KeyboardInterrupt:
        break

udp_transport.unbind()
