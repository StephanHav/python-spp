import spp
from spp.transport import UdpTransport


udp_transport = UdpTransport()

udp_transport.bind("127.0.0.1", 5444)

spp_entity = spp.SpacePacketProtocolEntity(apid=444, transport=udp_transport)


def display_received_packet(space_packet):
    print("Received Space Packet:", space_packet)
    attrs = vars(space_packet)
    print("   " + ", ".join("%s: %s" % item for item in attrs.items()))


spp_entity.indication = display_received_packet

input("Running. Press <Enter> to stop...\n")

udp_transport.unbind()
